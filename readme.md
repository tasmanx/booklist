#**Book List**

# ***Description***: #

### It's a simple Laravel 5.4 project which contains 2 view pages: list of books and info page about book. 
### Project has 5 db tables: books, authors, genre, author_book, book_genre.
### Features: pagination with maximum 5 books on page; books are sorted by alphabetical order; search form; seed data.

# ***Instructions to setup:*** #

### 1. Download repository or type 
```
#!python

git clone https://tasmanx@bitbucket.org/tasmanx/booklist.git
```
### 2. Rename .env.example file to .env
### 3. Enter Database and APP_KEY information for example:


```
#!python

APP_KEY=base64:9hDo6N81boBXxbClg3aU1ErmNTphafscR3ymiFf+pM8=

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=bookList
DB_USERNAME=root
DB_PASSWORD=
```
### 4. Open Command Prompt in project repository and type:

```
#!python

composer install
```
### 5. To create database tables type:
```
#!python

php artisan migrate
```
### 6. To seed database type:

```
#!python

php artisan db:seed
```
### 7. To run project type:

```
#!python

php artisan serve
```

# ***How to publish project to heroku:*** #
### 1. Login to heroku 

```
#!python

heroku login
```
### 2. Create laravel project or use existing one
### 3. Create 'Procfile' in your laravel project directory and type:

```
#!python

web: vendor/bin/heroku-php-apache2 public/
```
### 4. Initialize a Git repository (make sure .env is not in the .gitignore file):
```
#!python

git init
```
### 5. Create a heroku app and config php Buildpack:

```
#!python
heroku create
heroku config:set BUILDPACK_URL=https://github.com/heroku/heroku-buildpack-php
```
### 6. Add a postgres database:

```
#!python

heroku addons:add heroku-postgresql:hobby-dev
```
### 7. To get database parameters type:

```
#!python

heroku config
```
### You will get someting like this:
```
#!python

DATABASE_URL:         postgres://qufhdlhlaebpxm:5bcae76dd04367e5ef6853813e61d8343551c4b95ef4476dd91ec635c1fa1a0d@ec2-54-75-237-110.eu-west-1.compute.amazonaws.com:5432/df0974ketdqf8p
```

### 8. Update .env file:


```
#!python

DB_CONNECTION=pgsql
DB_HOST=ec2-54-75-237-110.eu-west-1.compute.amazonaws.com
DB_DATABASE=df0974ketdqf8p
DB_USERNAME=qufhdlhlaebpxm
DB_PASSWORD=5bcae76dd04367e5ef6853813e61d8343551c4b95ef4476dd91ec635c1fa1a0d
```
### 9. Commit and push to heroku:

```
#!python

git push heroku master
```
### 10. Create db tables:

```
#!python

heroku run php /app/artisan migrate

```
### 11. Seed db tables:

```
#!python

heroku run php /app/artisan db:seed
```
### 12. Open project:

```
#!python

heroku open
```