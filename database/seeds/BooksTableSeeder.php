<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
            'id' => 1,
            'title' => "Vilnius: meilės stotelės. Romantiškasis miesto žemėlapis",
            'year' => 2017,
        ]);

        DB::table('books')->insert(
            [
            'id' => 2,
            'title' => "Pape Satàn aleppe. Takios visuomenės kronikos",
            'year' => 2017,
        ]);

        DB::table('books')->insert(
            [
            'id' => 3,
            'title' => "Išgyvenimo patirtis. Antroji dalis",
            'year' => 2017,
        ]);

        DB::table('books')->insert(
            [
            'id' => 4,
            'title' => "Mano tėvas",
            'year' => 2017,
        ]);

        DB::table('books')->insert(
            [
            'id' => 5,
            'title' => "Paskutinis lapis",
            'year' => 2017,
        ]);

        DB::table('books')->insert(
            [
            'id' => 6,
            'title' => "Vilko vaikas: neįtikėtina Rytprūsių mergaitės Liesabeth Otto gyvenimo istorija",
            'year' => 2017,
        ]);

        DB::table('books')->insert(
            [
            'id' => 7,
            'title' => "Programavimo kalba C++",
            'year' => 2008,
        ]);

        DB::table('books')->insert(
            [
            'id' => 8,
            'title' => "Programavimas JAVA. Pirmoji pažintis",
            'year' => 2012,
        ]);

        DB::table('books')->insert(
            [
            'id' => 9,
            'title' => "Programavimas pradedantiesiems. Scratch",
            'year' => 2016,
        ]);

        DB::table('books')->insert(
            [
            'id' => 10,
            'title' => "Mažasis princas",
            'year' => 1943,
        ]);

        DB::table('books')->insert(
            [
            'id' => 11,
            'title' => "Eragonas",
            'year' => 2002,
        ]);

        DB::table('books')->insert(
            [
            'id' => 12,
            'title' => "Vyriausiasis",
            'year' => 2005,
        ]);

        DB::table('books')->insert(
            [
            'id' => 13,
            'title' => "Ugnies kardas",
            'year' => 2008,
        ]);

        DB::table('books')->insert(
            [
            'id' => 14,
            'title' => "Inheritance",
            'year' => 2011,
        ]);
 
    }
}
