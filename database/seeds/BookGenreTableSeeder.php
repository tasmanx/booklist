<?php

use Illuminate\Database\Seeder;

class BookGenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('book_genre')->insert(
            [
            'id' => 1,
            'book_id' => 1,
            'genre_id' => 1,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 2,
            'book_id' => 1,
            'genre_id' => 2,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 3,
            'book_id' => 2,
            'genre_id' => 1,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 4,
            'book_id' => 2,
            'genre_id' => 2,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 5,
            'book_id' => 3,
            'genre_id' => 1,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 6,
            'book_id' => 3,
            'genre_id' => 2,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 7,
            'book_id' => 4,
            'genre_id' => 2,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 8,
            'book_id' => 5,
            'genre_id' => 1,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 9,
            'book_id' => 5,
            'genre_id' => 2,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 10,
            'book_id' => 6,
            'genre_id' => 1,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 11,
            'book_id' => 6,
            'genre_id' => 2,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 12,
            'book_id' => 7,
            'genre_id' => 3,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 13,
            'book_id' => 7,
            'genre_id' => 4,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 14,
            'book_id' => 8,
            'genre_id' => 3,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 15,
            'book_id' => 8,
            'genre_id' => 5,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 16,
            'book_id' => 9,
            'genre_id' => 3,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 17,
            'book_id' => 10,
            'genre_id' => 6,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 18,
            'book_id' => 10,
            'genre_id' => 7,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 19,
            'book_id' => 11,
            'genre_id' => 7,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 20,
            'book_id' => 12,
            'genre_id' => 7,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 21,
            'book_id' => 13,
            'genre_id' => 7,
        ]);

        DB::table('book_genre')->insert(
            [
            'id' => 22,
            'book_id' => 14,
            'genre_id' => 7,
        ]);
    }
}
