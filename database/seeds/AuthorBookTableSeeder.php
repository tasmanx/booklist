<?php

use Illuminate\Database\Seeder;

class AuthorBookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('author_book')->insert(
            [
            'id' => 1,
            'author_id' => 1,
            'book_id' => 1,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 2,
            'author_id' => 2,
            'book_id' => 2,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 3,
            'author_id' => 3,
            'book_id' => 3,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 4,
            'author_id' => 4,
            'book_id' => 4,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 5,
            'author_id' => 5,
            'book_id' => 5,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 6,
            'author_id' => 6,
            'book_id' => 6,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 7,
            'author_id' => 7,
            'book_id' => 7,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 8,
            'author_id' => 8,
            'book_id' => 7,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 9,
            'author_id' => 9,
            'book_id' => 7,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 10,
            'author_id' => 10,
            'book_id' => 7,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 11,
            'author_id' => 11,
            'book_id' => 7,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 12,
            'author_id' => 12,
            'book_id' => 7,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 13,
            'author_id' => 13,
            'book_id' => 8,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 14,
            'author_id' => 14,
            'book_id' => 9,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 15,
            'author_id' => 15,
            'book_id' => 9,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 16,
            'author_id' => 16,
            'book_id' => 9,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 17,
            'author_id' => 17,
            'book_id' => 10,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 18,
            'author_id' => 18,
            'book_id' => 11,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 19,
            'author_id' => 18,
            'book_id' => 12,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 20,
            'author_id' => 18,
            'book_id' => 13,
        ]);

        DB::table('author_book')->insert(
            [
            'id' => 21,
            'author_id' => 18,
            'book_id' => 14,
        ]);
    }
}
