<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert(
            [
            'id' => 1,
            'name' => "Liuda",
            'surname' => "Matonienė",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 2,
            'name' => "Umberto",
            'surname' => "Eco",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 3,
            'name' => "Sergej",
            'surname' => "Lazarev",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 4,
            'name' => "Juan Pablo",
            'surname' => "Escobar",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 5,
            'name' => "Olivier",
            'surname' => "Truc",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 6,
            'name' => "Ingeborg",
            'surname' => "Jacobs",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 7,
            'name' => "Jonas",
            'surname' => "Blonskis",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 8,
            'name' => "Vytautas",
            'surname' => "Bukšnaitis",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 9,
            'name' => "Vacius",
            'surname' => "Jusas",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 10,
            'name' => "Romas",
            'surname' => "Marcinkevičius",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 11,
            'name' => "Alfonsas",
            'surname' => "Misevičius",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 12,
            'name' => "Sigita",
            'surname' => "Turskienė",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 13,
            'name' => "Aleksas",
            'surname' => "Riškus",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 14,
            'name' => "Rosie",
            'surname' => "Dickins",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 15,
            'name' => "Jonathan",
            'surname' => "Melmoth",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 16,
            'name' => "Louie",
            'surname' => "Stowell",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 17,
            'name' => "Antuanas de Sent",
            'surname' => "Egziuperi",
        ]);

        DB::table('authors')->insert(
            [
            'id' => 18,
            'name' => "Christopher",
            'surname' => "Paolini",
        ]);
    }
}
