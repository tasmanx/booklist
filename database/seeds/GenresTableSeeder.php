<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert(
            [
            'id' => 1,
            'name' => "Romanai",
        ]);

        DB::table('genres')->insert(
            [
            'id' => 2,
            'name' => "Detektyvai",
        ]);

        DB::table('genres')->insert(
            [
            'id' => 3,
            'name' => "Programavimas",
        ]);

        DB::table('genres')->insert(
            [
            'id' => 4,
            'name' => "C++",
        ]);

        DB::table('genres')->insert(
            [
            'id' => 5,
            'name' => "JAVA",
        ]);

        DB::table('genres')->insert(
            [
            'id' => 6,
            'name' => "Pasakos",
        ]);

        DB::table('genres')->insert(
            [
            'id' => 7,
            'name' => "Fantastika",
        ]);
    }
}
