<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class book extends Model
{
    public function authors() 
    {
        return $this->belongsToMany('app\author');
    }

    public function genres()
    {
        return $this->belongsToMany('app\genre');
    }
}
