<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <title>Informacinis puslapis</title>
    </head>
    <body>
      <table>
        <tr>
            <th>Knygos pavadinimas</th>
            <th>Leidimo metai</th>
            <th>Žandras(-ai)</th>
            <th>Autorius(-iai)</th>
        </tr>
        <tr>
            <td>{{ $book->title }}</td>
            <td>{{ $book->year }}</td>
            <td>
                @foreach ($book->genres as $genre)
                   {{ $genre->name }} </br>
                @endforeach
            </td>
            <td>
                @foreach ($book->authors as $author)
                   {{ $author->name }} {{ $author->surname }} </br>
                @endforeach
            </td>
            
        </tr>
      </table>
    </body>
</html>