<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <title>Knygų sąrašas</title>   
    </head>
    <body>
        <form action="/">
            <input type="text" name="search"/>
        </form>
        <ul>
            @foreach ($books as $book)
                <a href="/{{$book->id}}"><li>{{ $book->title }}</li></a>
            @endforeach
        </ul>
        {{ $books->links() }}
    </body>
</html>
