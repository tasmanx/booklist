<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\book;
use App\author;
use App\genre;

Route::get('/', function () {
    //$books = Book::orderBy('title', 'asc')->paginate(10);
    //return view('book', compact('books'));

    $search = Request::get("search");
    $books = Book::where('title', 'like', '%'.$search.'%')->orderBy('title', 'asc')->paginate(6);
    return view('book', compact('books'));
});

Route::get('/{book}', function ($id) {
    $book = Book::find($id);
    $author = Author::all();
    $genre = Genre::all();

    return view('info', compact('book'), compact('author'), compact('genre'));
});


